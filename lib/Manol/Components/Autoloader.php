<?php

 class Autoloader {
 	
 	private $ext = ".php";
 	
 	function __construct() {
 		
 		$this->register();	 	
 	}
 	
 	private function register() {
 		
 		spl_autoload_register(array($this, 'loadClass'));
 	}
 	
 	private function loadClass($className) {	
 		
 		if(!strpos($className, 'Controller')) {
 		$fileName = LIB_DIR . $className . $this->ext;
 		 
 		} else {
 			
 			$fileName = CONTROLLERS_DIR . $className . $this->ext;
 			
 		} 		
 	 
 		if(file_exists($fileName))
 			require $fileName;
 		else throw new Exception("The class " . $className . " does not exists");
 		 
 	}
 	
 	
 }
 
 $loader = new Autoloader();