<?php 
namespace Manol\Components;

	class View {
		
		private $layout = 'layout.php';
		private $incDir = "/../App/views/";
		function __construct() {
			
			
		}
		
		function decorate($body) {
			
			ob_start();
			$path = $this->incDir . $this->layout;
			require $path;	
			$content = ob_get_contents();
			
			ob_end_clean();
				
			return $content;
		}
		
	}