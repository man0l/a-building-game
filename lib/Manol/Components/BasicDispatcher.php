<?php 
namespace Manol\Components;

use Manol\Components\View;

 class BasicDispatcher {
 	
 	private $action = '';
 	private $view;
 	
 	function __construct($route) {
 		
 		$this->route = $route;
 		$this->action = $this->route->getAction();
 		$this->view = new View();
 		$this->process();
 	}
 	
 	
 	function process() {
 		
 		$methodName = $this->action . "Action";
 		if(method_exists($this, $methodName)) {
 			 call_user_func(array($this, $methodName));
 		} else throw new \Exception("The action ".$methodName ." does not exists in ".get_class($this));
 		
 		
 		$this->render();
 		
 	}
 	
 	function render() {
 		$output = $this->view->decorate($this->templateName);
 		
 		echo $output;
 		
 	}
 }