<?php 

namespace Manol\Components\Route;

class RouteMatch {
	
	private $controller = '';
	private $action = '';
	private $defaultController = 'game';
	private $defaultAction = 'index';
	
	private $params = array();
	
	function __construct($controllers = array()) {
 		// remove hostname and physical dir
 		$webPath = str_replace("http://".$_SERVER['HTTP_HOST'], "", WEB_PATH);
		$currentPath = str_replace($webPath, "", $_SERVER['REQUEST_URI']);	
		
		// the url structure is controller/action/{param1}/{param2}/{paramN}
		$routeElems = explode("/", $currentPath);
	 
		$this->controller 		= isset($routeElems[0]) ? $routeElems[0] : $this->defaultController;
		$this->action     		=  isset($routeElems[1]) ? $routeElems[1]: $this->defaultAction;
		$this->requestParams 	= array_slice($routeElems, 1);
		 
	}
	
	function getController() {
		return $this->controller;
	}
	
	function getAction() {
		return $this->action;		
	}
	
}