<?php 

namespace Manol\Components;

use Manol\Components\Route\RouteMatch;
use Manol\Components\Database;
use App\Controllers;

class Game {
	
	private $db;
	private $route;
	
	public function __construct() {
		
		$this->preProcess();
	}
	
	/**
	 * @desc Let's play the game
	 */
	public function play() {
		
		 
	}
	
	public function get($pattern, $callback) {
		
		
		// process results
		if(is_callable($callback)) {
			return call_user_func($callback);
		}
	}

	/**
	 * @desc we do not need a post functionality at this time
	 */
	public function post() {}
	
	private function match() {
		
	}
	
	private function preProcess() {
		
		$this->route = new RouteMatch();
		//$this->db = new Database();		
		
		$controllerClass = ucfirst($this->route->getController()) ."Controller";

		$controllerClass = "App\\Controllers\\".$controllerClass;
		$controller = new $controllerClass($this->route);
	 
	 
	}
	
	 
	
}