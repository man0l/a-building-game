<?php 

namespace Manol\Building;

	abstract class Building {
		
		/**
		 * @desc resource stone
		 * @var integer
		 */
		protected $stone;
		
		/**
		 * @desc resource wood
		 * @var integer
		 */
		protected $wood;
		
		/**
		 * @desc resource $iron
		 * @var integer
		 */
		protected $iron;
		
		protected $progress;
		protected $date;
		protected $level = 1;
		protected $price;
		protected $time;
		protected $difficult = 10;
		
		/**
		 * @desc time to build this resource in seconds
		 * @var integer
		 */
		protected $timeStone = 5;
		
		/**
		 * @desc time to build iron resource in seconds
		 * @var unknown
		 */
		protected $timeIron = 10;
		
		/**
		 * @desc time to build wood resource in seconds
		 * @var unknown
		 */
		protected $timeWood = 2;
		
		/**
		 * @desc the image file of the object 
		 * @var string
		 */
		protected $image;
		
		/**
		 * @desc create this building
		 */
		public function build() {}
		
		
		/**
		 * @desc visualise the building
		 */
		public function draw() {}
		
		/**
		 * @desc upgrade the building to the next level 
		 */
		public function upgrade() {}
		
		/**
		 * @desc calculate the price
		 * @return number
		 */
		public function getPrice() {
				$this->price = $this->level * $this->iron + 
							   $this->level * $this->wood + 
							   $this->level * $this->stone;	
				return $this->price;		
		}
		
		/**
		 * @desc calculate the time in seconds
		 * @return number
		 */
		public function getTime() {
				
			$coefStone = (($this->level * $this->stone) / $this->difficult) * $this->timeStone;
			$coefIron = (($this->level * $this->iron) / $this->difficult) * $this->timeIron;
			$coefWood = (($this->level * $this->wood) / $this->difficult) * $this->timeWood;

			$this->time = $coefStone + $coefIron + $coefWood;
			
			return $this->time;		
			
		} 
		
		
	}