<?php 

namespace Manol\Building;

use Manol\Building\Building;

class Barrack extends Building {
		  
		function __construct() {
			
			$this->wood = 30;
			$this->iron = 0;
			$this->stone = 20;
			
		}
	
} 