<?php
 
 define("LIB_DIR", __DIR__ ."/../lib/");
 define("CONTROLLERS_DIR", __DIR__."/../");
 define("DSN_STRING", "mysql:host=localhost;dbname=buildings");
 define("DB_USER", "root");
 define("DB_PASS", '');
 define("WEB_PATH", "http://localhost/a-building-game/web/");
 
 require LIB_DIR."/Manol/Components/Autoloader.php";
 
 
 
 use Manol\Components\Game;  

 
 $game = new Game;
  
 $game->play();